const router = require('express').Router();
const { checkUserInDatabase, login } = require('./../database/connect')
const bcrypt = require('bcrypt');


router.get('/',  function (req,res ) {
    res.render('login');
})


router.post('/', function (req, res) {
    if(req.session.auth ) {
        return res.redirect('/');
    }
    if(req.body.password === '') {
        return res.send('You send empty password');
    }
    checkUserInDatabase('MY_USERS', req.body.login)
        .then((c) => {
            if (c[0]['C'] === 1) {
                    login('MY_USERS', req.body.login)
                        .then(resultDB => {
                            //console.log(resultDB[0]['PASSWORD']);
                            bcrypt.compare(req.body.password, resultDB[0]['PASSWORD'], function(err, result) {
                                if(err) {
                                    res.send('hash wrong');
                                }
                                console.log(result);
                                console.log('resultDB[0][\'PASSWORD\']' + resultDB[0]['PASSWORD']);
                                console.log('hash' + req.body.password);
                                if(result) {
                                    console.log('resultDB[0][\'PASSWORD\']' + resultDB[0]['PASSWORD']);
                                    console.log('result' + result);
                                    req.session.auth = true;
                                    req.session.login = resultDB[0]['LOGIN'];
                                    res.setHeader('Access-Control-Allow-Credentials', 'true')
                                    req.session.save();
                                    res.redirect('/');
                                } else {
                                    res.send('Wrong password!');
                                }
                            });
                        })
                        .catch((error) => {
                            res.send('Not valid cridentials');
                        });
            } else {
                res.send('User not found');
            }
        }).catch((error) => {
            console.log(error);
            res.send('not valid cridentials');
        });
})


module.exports = router;