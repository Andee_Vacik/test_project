const router = require('express').Router();
const {  addEmp } = require('./../database/connect')

router.post('/',  function (req, res) {
        const body = req.body;
    //ename, job, mgr, hiredate, sal, comm, deptno
        addEmp(
            body.ENAME,
            body.JOB,
            body.MGR,
            body.HIREDATE,
            body.SAL,
            body.COMM,
            body.DEPTNO
        )
            .then( (data) => {
                console.log(data);
                res.send('Successfully added <a href="/">Click to return </a>');
            })
            .catch((error) => {
                console.log(error)
                res.send('Input is not valid or  try later')
            });
});
module.exports = router;