const router = require('express').Router();
const {  addDept } = require('./../database/connect')
const Tokens = require('csrf');


router.post('/',  function (req, res) {
    const body = req.body;
    console.log("req.tokens = " + req.session.tokens);
    let tokens = new Tokens();
    if(tokens.verify(req.session.secret, req.body.token)) {
        addDept(
            body.DNAME,
            body.LOC
            )
            .then( (data) => {
                console.log(data);
                res.send('Data successfully created! Please return back!');

            })
            .catch((error) => {
                console.log(error)
                res.send('Sorry try later')
            });
    } else {
        res.status(403).send('Token isn\'t valid');
    }
});
module.exports = router;