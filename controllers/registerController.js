const router = require('express').Router();
const { checkUserInDatabase, register } = require('./../database/connect')
const bcrypt = require('bcrypt');
const session = require('express-session');

router.get('/', function (req,res ) {
    res.render('register');
})


router.post('/', function (req, res) {
    console.log("req.body.password = " + req.body.password)
    if(req.session.auth ) {
        return res.redirect('/');
    }
    if(req.body.password === '') {
        return res.send('You send empty password');
    }
    checkUserInDatabase('MY_USERS', req.body.login)
        .then((c) => {
            console.log(req.body.login);
            console.log(c)
            console.log(req.body.password);

            if (c[0]['C'] === 0) {
                bcrypt.genSalt(5, function(err, salt) {
                    bcrypt.hash(req.body.password, salt, function (err, hash) {
                        console.log('hash = ' + hash);
                        register('MY_USERS', req.body.login, hash)
                            .then(result => {
                                console.log(result);
                                req.session.auth = true;
                                res.setHeader('Access-Control-Allow-Credentials', 'true')
                                req.session.save();
                                res.redirect('/');
                            })
                            .catch((error) => {
                                console.log(error);
                                res.send(error);
                            });
                    });
                });
            } else  {
                res.send('You already registered!');
            }
        }).catch((error) => {
            console.log(error);
            res.send('not valid cridentials');
        });
});
module.exports = router;