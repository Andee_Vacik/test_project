const router = require('express').Router();
const {  updatePassword } = require('./../database/connect')
const bcrypt = require('bcrypt');



router.post('/', function (req, res) {
    console.log(req.body.password);
    try {
        bcrypt.hash(req.body.password, 5, function (err, hash) {
            if(err) {
                console.log(err);
                res.send('Error with hash');
            }
            console.log(hash)
            updatePassword('MY_USERS', req.session.login, hash)
                .then((data) => {
                    console.log(data);
                    return res.status(200).end();
                })
                .catch((r) => {
                    console.log(r);
                    res.send('Error occured with database')
                });
        });
    } catch (e) {
        console.log(e)
        res.send('Sorry try later')
    }

});
module.exports = router;