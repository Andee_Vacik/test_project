const config = require('./config');
var knex = require('knex')(config);

/*const setupPaginator = require('knex-paginator');
setupPaginator(knex);*/

const connect = (tableName, offset) => {
    return knex.select('*')
        .from(tableName).limit(5).offset((offset - 1) * 5);
}



const count = (tableName) => {
    /*return knex.select('*')
        .from(tableName);*/
    return knex(tableName).count(tableName + 'NO' + ' as ' + 'CNT' + tableName);
}

const register = (tableName, login, password) => {
    return knex(tableName).insert({
            'LOGIN': login,
            'PASSWORD': password
        });
}

const checkUserInDatabase = (tableName, login) => {
    return knex.table(tableName)
        .where('LOGIN',  login).count('* as C').select();
}


const login = (tableName, login) => {
    return knex.select('*').from(tableName).where({
        'LOGIN': login
    });
}

const updatePassword = (tableName, login, password) => {
    return knex(tableName).where({ 'LOGIN': login })
        .update({ 'PASSWORD': password });
}

const addEmp = (ename, job, mgr, hiredate, sal, comm, deptno) => {
    return knex.raw(`insert into EMP(EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO ) 
        values(semp.nextval, ${"'"+ename+"'"}, ${"'"+job+"'"}, ${mgr}, to_date('${hiredate}', 'DD-MM-YYYY'), ${sal} , ${comm}, ${deptno} ) 
    `);
}

const addDept = (dname, loc) => {
    return knex.raw(`insert into DEPT(DEPTNO, DNAME, LOC ) 
        values(sdept.nextval, ${"'" + dname+"'"}, ${"'"+loc+"'"})`);
}

module.exports = { count, connect, login, register, checkUserInDatabase, updatePassword, addEmp, addDept };