const express = require('express');
const  { count, connect } = require('./database/connect');
const loginController = require('./controllers/loginController');
const session = require('express-session');
const { uuid: uuidv4 } = require('uuid');
const  FileStore = require('session-file-store')(session);
const app = express();
const cookieParser = require('cookie-parser');
const registerController = require('./controllers/registerController');
const changePasswordController = require('./controllers/changePasswordController');
const path = require('path');
const addempController = require('./controllers/addempController')
const adddeptController = require('./controllers/adddeptController')
const Tokens = require('csrf');
const helmet = require('helmet');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(helmet())
//  Ejc
//app.engine('ejs', require('ejs-locals'));
app.set('view engine', 'ejs');
//app.set('views', 'templates');

app.use(session({
    name: 'server-session',
    secret: 'my express secret',
    resave: false,
    saveUninitialized: false,
    /*saveUninitialized: true,
    resave: true,*/
    store: new FileStore()
}))


const empTableName = 'EMP';
const deptTableName = 'DEPT';

app.use('/register', registerController);

app.use('/login', loginController);

app.use('/change', changePasswordController);

app.use('/addemp', addempController);

app.use('/adddept', adddeptController);

const tokens = new Tokens();


app.get('/', async function(req, res){
    console.log('req.session.auth = ', req.session.auth);
    if(req.session.auth) {

        //crsf
        let secret = tokens.secretSync();
        req.session.secret = secret;
        let tokenEmp = tokens.create(secret);
        /*  Get employees with validation    */
        let empCount = await count(empTableName);
        empCount = empCount[0]['CNT' + empTableName];
        let currentPageEmp = req.query.pageemp || 1;
        if (Math.ceil((empCount / 5)) < currentPageEmp) {
            currentPageEmp = 1;
        }
        if (currentPageEmp < 0) {
            currentPageEmp = 1;
        }
        const employees = await connect('EMP', currentPageEmp);
        /*  Get employees with validation    */


        let deptCount = await count(deptTableName);
        deptCount = deptCount[0]['CNT' + deptTableName];
        let currentPageDept = req.query.pagedept || 1;
        if (Math.ceil((deptCount / 5)) < currentPageDept) {
            currentPageDept = 1;
        }
        if (currentPageDept < 0) {
            currentPageDept = 1;
        }
        const depts = await connect('DEPT', currentPageDept);


        let tokenDept = tokens.create(secret);

        res.render("index", {
            employees: employees,
            currentPageEmp: currentPageEmp,
            empCount: Math.ceil(empCount / 5),
            tokenEmp: tokenEmp,

            depts: depts,
            currentPageDept: currentPageDept,
            deptCount: Math.ceil(deptCount / 5),
            tokenDept: tokenDept
        });
    }
    res.send('Authorize');
});



app.listen(3000, function() {
    console.log('Example app listening on port 3000!')
})

